# Barèmes IPP Edit

_Web editor for French legislation parameters_

**THIS PROJECT HAS MIGRATED TO https://gitlab.com/ipp/partage-public-ipp/baremes-ipp/baremes-ipp-edit.**

## Installing

```bash
git clone https://gitlab.com/ipp/partage-public-ipp/baremes-ipp/baremes-ipp-edit.git
cd baremes-ipp-edit/
npm install
```

Create a `.env` file:

```bash
cp example.env .env
```

Then adapt this `.env` file to your needs.

## Developing

```bash
npm run dev

# or start the server and open the app in a new browser tab
npm run dev -- --open
```

## Building

To create a production version of your app:

```bash
npm run build
```

You can preview the production build with `npm run preview`.
