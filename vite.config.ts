import { sveltekit } from "@sveltejs/kit/vite"
import morgan from "morgan"
import type { UserConfig } from "vite"

const middlewaresPlugin = {
  name: "baremes-ipp-edit-middlewares",
  async configureServer(server) {
    server.middlewares.use(morgan("dev"))
  },
}

const config: UserConfig = {
  plugins: [sveltekit(), middlewaresPlugin],
}

export default config
