// See https://kit.svelte.dev/docs/types#app
// for information about these interfaces
type Unit = import("@tax-benefit/openfisca-json-model").Unit

declare global {
  namespace App {
    // interface Error {}
    // interface Locals {}
    interface PageData {
      appTitle: string
      hasRespositoryToken: boolean
      parametersRepository: import("$lib/repositories").RepositoryConfig
      rootParameter: Parameter
      unitByName: { [name: string]: Unit }
      units: Unit[]
    }
    // interface Platform {}
}

export {}
