import { getEmbeddedAndFileParameters } from "$lib/file_parameters"

import type { LayoutLoad } from "./$types"

export const load: LayoutLoad = async ({ params, parent }) => {
  const { parametersRepository, rootParameter, units } = await parent()
  const { parameter: name } = params
  return await getEmbeddedAndFileParameters(
    rootParameter,
    name,
    parametersRepository,
    units,
  )
}
