import {
  auditSetNullish,
  auditSingleton,
  auditTrimString,
  cleanAudit,
  type Audit,
} from "@auditors/core"
import { error } from "@sveltejs/kit"

import type { PageLoad } from "./$types"

function auditQuery(audit: Audit, query: URLSearchParams): [unknown, unknown] {
  if (query == null) {
    return [query, null]
  }
  if (!(query instanceof URLSearchParams)) {
    return audit.unexpectedType(query, "URLSearchParams")
  }

  const data: { [key: string]: unknown } = {}
  for (const [key, value] of query.entries()) {
    let values = data[key] as string[] | undefined
    if (values === undefined) {
      values = data[key] = []
    }
    values.push(value)
  }
  const errors: { [key: string]: unknown } = {}
  const remainingKeys = new Set(Object.keys(data))

  for (const key of ["merge_request_url", "warning"]) {
    audit.attribute(
      data,
      key,
      true,
      errors,
      remainingKeys,
      auditSingleton(auditTrimString),
    )
  }

  return audit.reduceRemaining(data, errors, remainingKeys, auditSetNullish({}))
}

export const load: PageLoad = async ({ params, parent, url }) => {
  const [query, queryError] = auditQuery(cleanAudit, url.searchParams) as [
    { merge_request_url?: string; warning?: string },
    unknown,
  ]
  if (queryError !== null) {
    console.error(
      `Error in ${url.pathname} search parameters:\n${JSON.stringify(
        query,
        null,
        2,
      )}\n\nError:\n${JSON.stringify(queryError, null, 2)}`,
    )
    throw error(
      400,
      `Invalid search parameters:\n${JSON.stringify(
        query,
        null,
        2,
      )}\n\nError:\n${JSON.stringify(queryError, null, 2)}`,
    )
  }

  return {
    ...query,
  }
}
