import { error } from "@sveltejs/kit"

import type { PageLoad } from "./$types"

export const load: PageLoad = async function ({ params, parent }) {
  const { embeddedParameter, fileParameter } = await parent()

  if (fileParameter === undefined) {
    throw error(
      400,
      `L'édition du paramètre "${
        embeddedParameter.name ?? "racine"
      }" est impossible car c'est un nœud généré automatiquement, qui n'a pas de fichier source.`,
    )
  }

  return {}
}
