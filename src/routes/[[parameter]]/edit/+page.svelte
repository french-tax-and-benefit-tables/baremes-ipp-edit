<script lang="ts">
  import "prismjs/themes/prism.css"

  import { laxAudit } from "@auditors/core"
  import mdiFileTreeOutline from "@iconify-icons/mdi/file-tree-outline"
  import mdiSend from "@iconify-icons/mdi/send"
  import {
    iterParameterAncestors,
    ParameterClass,
    yamlFromRawParameter,
    type Parameter,
  } from "@tax-benefit/openfisca-json-model"
  import {
    Accordion,
    AccordionItem,
    Breadcrumb,
    BreadcrumbItem,
    Button,
    Card,
    Helper,
    Input,
    Label,
    Select,
    Textarea,
  } from "flowbite-svelte"
  import { getContext } from "svelte"
  import type { Writable } from "svelte/store"

  import { goto } from "$app/navigation"
  import Prism from "$lib/components/Prism.svelte"
  // Must be imported after Prism component:
  import "prismjs/components/prism-yaml"
  import NodeEdit from "$lib/components/parameters/NodeEdit.svelte"
  import ScaleEdit from "$lib/components/parameters/ScaleEdit.svelte"
  import ValueEdit from "$lib/components/parameters/ValueEdit.svelte"
  import {
    auditEditableParameterStricter,
    convertEditableParameterToRawAndClean,
  } from "$lib/file_parameters"
  import { asNodeParameter, labelFromParameterClass } from "$lib/parameters"

  import type { PageData } from "./$types"

  export let data: PageData

  const date = getContext("date") as Writable<string>
  let { embeddedParameter, fileParameter, hasRespositoryToken } = data

  let errors: { [key: string]: unknown } = {}
  let originalParameter = { ...fileParameter! }
  let parameter = fileParameter!
  if (parameter.description === undefined) {
    parameter.description = parameter.title
  }
  let reviewed = false
  let showErrors = false
  let validRunParameter: Parameter | undefined = undefined // "Run" means series (=> value parameter)

  $: runRawParameter =
    validRunParameter === undefined
      ? undefined
      : convertEditableParameterToRawAndClean(validRunParameter)

  $: runRawParameterYaml =
    runRawParameter === undefined
      ? undefined
      : yamlFromRawParameter(runRawParameter)

  $: ({ units } = data)

  $: if (reviewed) {
    parameter = {
      ...parameter,
      last_value_still_valid_on: new Date().toISOString().split("T")[0],
    }
  } else {
    parameter = {
      ...parameter,
      last_value_still_valid_on: originalParameter.last_value_still_valid_on,
    }
  }

  $: ((parameter) => {
    const [validParameter, parameterError] = auditEditableParameterStricter(
      units,
      // Since parameter YAML file (most of the times) doesn't contain the children,
      // inject the children IDs to be able to validate `order` metadata.
      embeddedParameter.class === ParameterClass.Node &&
        embeddedParameter.children !== undefined
        ? Object.keys(embeddedParameter.children)
        : undefined,
    )(laxAudit, parameter)
    errors = (parameterError as { [key: string]: unknown } | null) ?? {}
    validRunParameter =
      parameterError === null ? (validParameter as Parameter) : undefined
  })(parameter)

  async function save() {
    const [validParameter, parameterError] = auditEditableParameterStricter(
      units,
      // Since parameter YAML file (most of the times) doesn't contain the children,
      // inject the children IDs to be able to validate `order` metadata.
      embeddedParameter.class === ParameterClass.Node &&
        embeddedParameter.children !== undefined
        ? Object.keys(embeddedParameter.children)
        : undefined,
    )(laxAudit, parameter)
    if (parameterError !== null) {
      showErrors = true
      return
    }
    showErrors = false
    const urlString = `/${parameter.name}`
    const response = await fetch(urlString, {
      body: JSON.stringify(validParameter),
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json; charset=utf-8",
      },
      method: "PUT",
    })
    if (response.ok) {
      errors = {}
      const result = await response.json()
      const searchParams = new URLSearchParams(
        result.warning === undefined
          ? { merge_request_url: result.web_url }
          : result,
      )
      await goto(`/${parameter.name}?${searchParams.toString()}`)
    } else {
      console.error(
        `Error ${response.status} while updating parameter "${
          parameter.name
        }" at ${urlString}\n\n${await response.text()}`,
      )
      errors = { header: "L'enregistrement du paramètre a échoué." }
      showErrors = true
    }
  }
</script>

<svelte:head>
  <title>Édition | {parameter.name} | Paramètres | {data.appTitle}</title>
</svelte:head>

<main class="container mx-auto">
  <Breadcrumb aria-label="Hiérarchie d'accès à cette page">
    <BreadcrumbItem href="/" home>
      <iconify-icon
        class="mr-1 align-[-0.3rem] text-lg"
        icon={mdiFileTreeOutline}
        slot="icon"
      />
      Paramètres</BreadcrumbItem
    >
    {#each [...iterParameterAncestors(embeddedParameter)] as ancestor}
      <BreadcrumbItem href="/{ancestor.name}"
        >{ancestor.short_label ??
          ancestor.id?.replace(/_/g, " ")}</BreadcrumbItem
      >
    {/each}
    <BreadcrumbItem>Édition</BreadcrumbItem>
  </Breadcrumb>

  <hgroup class="my-4">
    <h2 class="text-3xl font-bold dark:text-white">
      Édition du paramètre <i>{parameter.title}</i>
    </h2>
    {#if parameter.description !== undefined && parameter.description !== parameter.title}
      <h3 class="text-lg font-normal text-gray-500 dark:text-gray-400">
        {parameter.description}
      </h3>
    {/if}
  </hgroup>

  {#if showErrors && Object.keys(errors).length > 0}
    <div
      class="border-l-4 border-red-500 bg-red-100 p-4 text-red-700"
      role="alert"
    >
      <p class="font-bold">Error</p>
      {#if typeof errors === "string"}
        <p>{errors}</p>
      {:else if errors.header !== undefined}
        <p>{errors.header}</p>
      {:else}
        <dl>
          {#each Object.entries(errors) as [key, message]}
            <dt>{key}</dt>
            <dd>{message}</dd>
          {/each}
        </dl>
      {/if}
    </div>
    <pre>{JSON.stringify(errors, null, 2)}</pre>
  {/if}

  <form on:submit|preventDefault={save}>
    <Card class="mt-4" size="xl" padding="xl">
      <h3 class="pb-3 font-serif text-3xl">Intitulés</h3>

      <div class="mb-6">
        <Label for="description">Nom complet<sup>*</sup></Label>
        <Textarea
          name="description"
          required={showErrors}
          bind:value={parameter.description}
        />
        <Helper
          >Ce libellé doit permettre à lui seul de comprendre précisément le
          rôle de ce paramètre.</Helper
        >
        {#if showErrors && errors.description !== undefined}
          <Helper color="red">{errors.description}</Helper>
        {/if}
      </div>

      <div class="mb-6">
        <Label for="label_en">Nom complet en anglais 🇬🇧</Label>
        <Textarea name="label_en" bind:value={parameter.label_en} />
        {#if showErrors && errors.label_en !== undefined}
          <Helper color="red">{errors.label_en}</Helper>
        {/if}
      </div>

      <div class="mb-6 grid grid-cols-2 gap-4">
        <div>
          <Label for="short_label">Nom court</Label>
          <Input
            name="short_label"
            type="text"
            bind:value={parameter.short_label}
          />
          <Helper
            >Quand il est précédé des noms courts des paramètres parents, le nom
            court doit permettre de comprendre précisément le rôle de ce
            paramètre.</Helper
          >
          {#if showErrors && errors.short_label !== undefined}
            <Helper color="red">{errors.short_label}</Helper>
          {/if}
        </div>

        <div>
          <Label for="short_label_en">Nom court en anglais 🇬🇧</Label>
          <Input
            name="short_label_en"
            type="text"
            bind:value={parameter.short_label_en}
          />
          {#if showErrors && errors.short_label_en !== undefined}
            <Helper color="red">{errors.short_label_en}</Helper>
          {/if}
        </div>
      </div>

      <div>
        <Label for="documentation">Documentation</Label>
        <Textarea
          name="documentation"
          rows="5"
          bind:value={parameter.documentation}
        />
        {#if showErrors && errors.documentation !== undefined}
          <Helper color="red">{errors.documentation}</Helper>
        {/if}
      </div>
    </Card>

    <Card class="mt-4" size="xl" padding="xl">
      <h3 class="pb-3 font-serif text-3xl">Valeurs</h3>

      <div class="mb-6">
        <Label for="class">Type de paramètre</Label>
        <Select
          disabled
          items={Object.values(ParameterClass).map((parameterClass) => ({
            name: labelFromParameterClass(parameterClass),
            value: parameterClass,
          }))}
          name="class"
          bind:value={parameter.class}
        />
        {#if showErrors && errors.class !== undefined}
          <Helper color="red">{errors.class}</Helper>
        {/if}
      </div>

      {#if parameter.class === ParameterClass.Node}
        <NodeEdit
          embeddedParameter={asNodeParameter(embeddedParameter)}
          {errors}
          bind:parameter
          {showErrors}
        />
      {:else if parameter.class === ParameterClass.Scale}
        <ScaleEdit {errors} bind:parameter bind:reviewed {showErrors} />
      {:else if parameter.class === ParameterClass.Value}
        <ValueEdit {errors} bind:parameter bind:reviewed {showErrors} />
      {/if}
    </Card>

    <Card class="mt-4" size="xl" padding="xl">
      <h3 class="pb-3 font-serif text-3xl">Envoi de la proposition</h3>

      <Accordion>
        <AccordionItem>
          <span slot="header">Voir le code généré au format YAML</span>
          {#if Object.keys(errors).length > 0}
            <div
              class="border-l-4 border-red-500 bg-red-100 p-4 text-red-700"
              role="alert"
            >
              <p class="font-bold">Error</p>
              {#if typeof errors === "string"}
                <p>{errors}</p>
              {:else if errors.header !== undefined}
                <p>{errors.header}</p>
              {:else}
                <dl>
                  {#each Object.entries(errors) as [key, message]}
                    <dt>{key}</dt>
                    <dd>{message}</dd>
                  {/each}
                </dl>
              {/if}
            </div>
            <pre>{JSON.stringify(errors, null, 2)}</pre>
          {/if}
          {#if runRawParameterYaml !== undefined}
            <Prism language="yaml" source={runRawParameterYaml} />
          {/if}
        </AccordionItem>
      </Accordion>

      <div
        class="mt-4 items-center justify-center space-y-4 sm:flex sm:space-y-0 sm:space-x-4"
      >
        <Button
          disabled={!hasRespositoryToken || parameter.file_path === undefined}
          type="submit"
        >
          <iconify-icon class="mr-2" icon={mdiSend} />Envoyer
        </Button>
      </div>
    </Card>
  </form>
</main>
