import { strictAudit } from "@auditors/core"
import {
  ParameterClass,
  yamlFromRawParameter,
  type Parameter,
} from "@tax-benefit/openfisca-json-model"
import { error, json } from "@sveltejs/kit"
import { randomBytes } from "crypto"

import {
  auditEditableParameterStricter,
  convertEditableParameterToRawAndClean,
  getEmbeddedAndFileParameters,
} from "$lib/file_parameters"
import config from "$lib/server/config"
import { rootParameter } from "$lib/server/parameters"
import { units } from "$lib/server/units"

import type { RequestHandler } from "./$types"

const { gitlabAccessToken, parametersRepository } = config

export const PUT: RequestHandler = async ({ request, params }) => {
  if (gitlabAccessToken === undefined) {
    throw error(
      403,
      `L'édition de paramètres est impossible car le serveur n'a pas de jeton d'accès au projet https//${parametersRepository.forge}/${parametersRepository.group}/${parametersRepository.project}.`,
    )
  }

  const { parameter: name } = params
  const { embeddedParameter, fileParameter, rawFileParameterYaml } =
    await getEmbeddedAndFileParameters(
      rootParameter,
      name,
      parametersRepository,
      units,
    )

  if (fileParameter === undefined || rawFileParameterYaml === undefined) {
    throw error(
      400,
      `L'édition du paramètre "${name}" est impossible car le paramètre est généré automatiquement et n'a pas de fichier source.`,
    )
  }

  const [validParameter, parameterError] = auditEditableParameterStricter(
    units, // Since parameter YAML file (most of the times) doesn't contain the children,
    // inject the children IDs to be able to validate `order` metadata.
    embeddedParameter.class === ParameterClass.Node &&
      embeddedParameter.children !== undefined
      ? Object.keys(embeddedParameter.children)
      : undefined,
  )(strictAudit, await request.json())
  if (parameterError !== null) {
    throw error(400, JSON.stringify(parameterError, null, 2))
  }

  const rawParameter = convertEditableParameterToRawAndClean(
    validParameter as Parameter,
  )
  const rawParameterYaml = yamlFromRawParameter(rawParameter)
  if (rawFileParameterYaml.trim() === rawParameterYaml.trim()) {
    // Parameter is not modified.
    return json({
      warning:
        "Le paramètre n'a pas été enregistré, car il n'avait aucune modification.",
    })
  }

  // Create new branch.
  const newBranchName = `edit_param_${name}_${randomBytes(4).toString("hex")}`
  const newBranchUrl = `https://${
    parametersRepository.forge
  }/api/v4/projects/${encodeURIComponent(
    `${parametersRepository.group}/${parametersRepository.project}`,
  )}/repository/branches?branch=${newBranchName}&ref=${encodeURIComponent(
    parametersRepository.branch,
  )}`
  const newBranchResponse = await fetch(newBranchUrl, {
    body: "",
    headers: {
      Accept: "application/json",
      "PRIVATE-TOKEN": gitlabAccessToken,
    },
    method: "POST",
  })
  if (!newBranchResponse.ok) {
    throw error(newBranchResponse.status, await newBranchResponse.text())
  }

  // Update parameter & commit it.
  const parameterUpdateUrl = `https://${
    parametersRepository.forge
  }/api/v4/projects/${encodeURIComponent(
    `${parametersRepository.group}/${parametersRepository.project}`,
  )}/repository/files/${encodeURIComponent(embeddedParameter.file_path!)}`
  const parameterUpdateResponse = await fetch(parameterUpdateUrl, {
    body: JSON.stringify(
      {
        author_email: parametersRepository.authorEmail,
        author_name: parametersRepository.authorName,
        branch: newBranchName,
        commit_message: `Modification du paramètre ${name}`,
        content: rawParameterYaml,
      },
      null,
      2,
    ),
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      "PRIVATE-TOKEN": gitlabAccessToken,
    },
    method: "PUT",
  })
  if (!parameterUpdateResponse.ok) {
    throw error(
      parameterUpdateResponse.status,
      await parameterUpdateResponse.text(),
    )
  }
  /* const parameterUpdate = */ await parameterUpdateResponse.json()

  // Create merge request.
  const mergeRequestUrl = `https://${
    parametersRepository.forge
  }/api/v4/projects/${encodeURIComponent(
    `${parametersRepository.group}/${parametersRepository.project}`,
  )}/merge_requests`
  const mergeRequestResponse = await fetch(mergeRequestUrl, {
    body: JSON.stringify(
      {
        allow_collaboration: true,
        remove_source_branch: true,
        source_branch: newBranchName,
        target_branch: parametersRepository.branch,
        title: `Modification du paramètre ${name}`,
      },
      null,
      2,
    ),
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      "PRIVATE-TOKEN": gitlabAccessToken,
    },
    method: "POST",
  })
  if (!mergeRequestResponse.ok) {
    throw error(mergeRequestResponse.status, await mergeRequestResponse.text())
  }
  const mergeRequest = await mergeRequestResponse.json()
  return json(mergeRequest)
}
