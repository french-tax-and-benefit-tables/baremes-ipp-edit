// import type { Parameter, Unit } from "@tax-benefit/openfisca-json-model"

import config from "$lib/server/config"
import { rootParameter } from "$lib/server/parameters"
import { unitByName, units } from "$lib/server/units"

import type { LayoutServerLoad } from "./$types"

const { gitlabAccessToken, parametersRepository, title: appTitle } = config

export const load: LayoutServerLoad = (): App.PageData => {
  return {
    appTitle,
    hasRespositoryToken: gitlabAccessToken !== undefined,
    parametersRepository,
    rootParameter,
    unitByName,
    units,
  }
}
