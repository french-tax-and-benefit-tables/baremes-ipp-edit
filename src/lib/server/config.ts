import "dotenv/config"

import type { RepositoryConfig } from "$lib/repositories"
import { validateConfig } from "$lib/server/auditors/config"

export interface Config {
  gitlabAccessToken?: string
  parametersDir: string
  parametersRepository: RepositoryConfig
  title: string
  unitsFilePath: string
}

const [config, error] = validateConfig({
  gitlabAccessToken: process.env["GITLAB_ACCESS_TOKEN"],
  parametersDir: process.env["PARAMETERS_DIR"],
  parametersRepository: {
    authorEmail: process.env["PARAMETERS_AUTHOR_EMAIL"],
    authorName: process.env["PARAMETERS_AUTHOR_NAME"],
    branch: process.env["PARAMETERS_BRANCH"],
    forge: process.env["PARAMETERS_FORGE"],
    group: process.env["PARAMETERS_GROUP"],
    parametersDir: process.env["PARAMETERS_PROJECT_DIR"],
    project: process.env["PARAMETERS_PROJECT"],
  },
  title: process.env["TITLE"],
  unitsFilePath: process.env["UNITS_FILE_PATH"],
}) as [Config, unknown]
if (error !== null) {
  console.error(
    `Error in configuration:\n${JSON.stringify(
      config,
      null,
      2,
    )}\nError:\n${JSON.stringify(error, null, 2)}`,
  )
  process.exit(-1)
}

export default config
