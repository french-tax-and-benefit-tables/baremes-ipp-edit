import { auditChain, auditRequire, strictAudit } from "@auditors/core"
import {
  auditRawParameterToEditable,
  improveParameter,
  rawParameterFromYaml,
  type Parameter,
} from "@tax-benefit/openfisca-json-model"
import fs from "fs-extra"
import path from "path"

import config from "$lib/server/config"
import { units } from "$lib/server/units"

const { parametersDir, parametersRepository } = config

export const rootParameter = loadParameters(parametersDir)
improveParameter(rootParameter)

function loadParameters(parametersDir: string): Parameter {
  const rootRawParameter = loadRawParameters(parametersDir)
  const [editableParameter, error] = auditChain(
    auditRawParameterToEditable(units),
    auditRequire,
  )(strictAudit, rootRawParameter) as [Parameter, unknown]
  if (error !== null) {
    console.error(
      `An error occurred when converting raw parameters at ${parametersDir} to editable:`,
    )
    console.error(JSON.stringify(editableParameter, null, 2))
    console.error(JSON.stringify(error, null, 2))
    throw new Error("Conversion of raw parameters to editable failed.")
  }
  return editableParameter
}

function loadRawParameters(
  parametersDir: string,
  relativeSplitPath: string[] = [],
): unknown {
  const nodePath = path.join(parametersDir, ...relativeSplitPath)
  let rawParameter: { [key: string]: unknown }
  if (fs.statSync(nodePath).isDirectory()) {
    const indexFilePath = path.join(nodePath, "index.yaml")
    if (fs.pathExistsSync(indexFilePath)) {
      rawParameter = rawParameterFromYaml(
        fs.readFileSync(indexFilePath, "utf-8"),
      ) as { [key: string]: unknown }
      rawParameter.file_path = path.join(
        parametersRepository.parametersDir,
        ...relativeSplitPath,
        "index.yaml",
      )
    } else {
      rawParameter = {}
    }
    for (const childFilename of fs.readdirSync(nodePath)) {
      if (childFilename[0] === "." || childFilename === "index.yaml") {
        continue
      }
      const childRelativeSplitPath = [...relativeSplitPath, childFilename]
      const child = loadRawParameters(parametersDir, childRelativeSplitPath)
      const childId = childFilename.replace(/\.yaml$/, "")
      ;(rawParameter as { [key: string]: unknown })[childId] = child
    }
  } else {
    rawParameter = rawParameterFromYaml(fs.readFileSync(nodePath, "utf-8")) as {
      [key: string]: unknown
    }
    rawParameter.file_path = path.join(
      parametersRepository.parametersDir,
      ...relativeSplitPath,
    )
  }
  const name = relativeSplitPath
    .join(".")
    .replace(/\.yaml$/, "")
    .replace(/\.index$/, "")
  ;(rawParameter as { [key: string]: unknown }).name = name
  return rawParameter
}
