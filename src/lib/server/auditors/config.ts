import {
  auditRequire,
  auditTrimString,
  cleanAudit,
  type Audit,
} from "@auditors/core"

export function auditConfig(
  audit: Audit,
  dataUnknown: unknown,
): [unknown, unknown] {
  if (dataUnknown == null) {
    return [dataUnknown, null]
  }
  if (typeof dataUnknown !== "object") {
    return audit.unexpectedType(dataUnknown, "object")
  }

  const data = { ...dataUnknown }
  const errors: { [key: string]: unknown } = {}
  const remainingKeys = new Set(Object.keys(data))

  for (const key of ["gitlabAccessToken"]) {
    audit.attribute(data, key, true, errors, remainingKeys, auditTrimString)
  }
  for (const key of ["parametersDir", "title", "unitsFilePath"]) {
    audit.attribute(
      data,
      key,
      true,
      errors,
      remainingKeys,
      auditTrimString,
      auditRequire,
    )
  }
  audit.attribute(
    data,
    "parametersRepository",
    true,
    errors,
    remainingKeys,
    auditRepositoryConfig,
    auditRequire,
  )

  return audit.reduceRemaining(data, errors, remainingKeys)
}

export function validateConfig(data: unknown): [unknown, unknown] {
  return auditConfig(cleanAudit, data)
}

export function auditRepositoryConfig(
  audit: Audit,
  dataUnknown: unknown,
): [unknown, unknown] {
  if (dataUnknown == null) {
    return [dataUnknown, null]
  }
  if (typeof dataUnknown !== "object") {
    return audit.unexpectedType(dataUnknown, "object")
  }

  const data = { ...dataUnknown }
  const errors: { [key: string]: unknown } = {}
  const remainingKeys = new Set(Object.keys(data))

  for (const key of [
    "authorEmail",
    "authorName",
    "branch",
    "forge",
    "group",
    "parametersDir",
    "project",
  ]) {
    audit.attribute(
      data,
      key,
      true,
      errors,
      remainingKeys,
      auditTrimString,
      auditRequire,
    )
  }

  return audit.reduceRemaining(data, errors, remainingKeys)
}
