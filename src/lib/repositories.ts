import type { Parameter } from "@tax-benefit/openfisca-json-model"

export interface RepositoryConfig {
  authorName: string
  authorEmail: string
  branch: string
  forge: string
  group: string
  parametersDir: string
  project: string
}

export function newParameterRepositoryRawUrl(
  { branch, forge, group, project }: RepositoryConfig,
  parameter: Parameter,
): string | undefined {
  const path = parameter.file_path
  if (path === undefined) {
    return undefined
  }
  return `https://${forge}/${group}/${project}/-/raw/${branch}/${path}`
}

export function newParameterRepositoryUrl(
  { branch, forge, group, project }: RepositoryConfig,
  parameter: Parameter,
): string | undefined {
  const path = parameter.file_path
  if (path === undefined) {
    return undefined
  }
  return `https://${forge}/${group}/${project}/-/blob/${branch}/${path}`
}
