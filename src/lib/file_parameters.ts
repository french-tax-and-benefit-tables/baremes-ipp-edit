import {
  Audit,
  auditChain,
  auditRequire,
  strictAudit,
  type Auditor,
} from "@auditors/core"
import {
  auditEditableParameter,
  auditRawParameterToEditable,
  convertEditableParameterToRaw,
  ParameterClass,
  rawParameterFromYaml,
  type Parameter,
  type Unit,
} from "@tax-benefit/openfisca-json-model"
import { error } from "@sveltejs/kit"

import { browser } from "$app/environment"
import { getEmbeddedParameter } from "$lib/parameters"
import type { RepositoryConfig } from "$lib/repositories"

export function auditEditableParameterStricter(
  units: Unit[],
  childrenId?: string[] | undefined,
): Auditor {
  return function (audit: Audit, data: unknown): [unknown, unknown] {
    const [parameter, error] = auditEditableParameter(units, childrenId)(
      audit,
      data,
    )

    const errors: { [key: string]: unknown } = error === null ? {} : error
    const remainingKeys = new Set<string>()

    if (errors.description === undefined) {
      audit.attribute(
        parameter,
        "description",
        true,
        errors,
        remainingKeys,
        auditRequire,
      )
    }

    return audit.reduceErrors(parameter, errors)
  }
}

export function convertEditableParameterToRawAndClean(
  editableParameter: Parameter,
): {
  [key: string]: unknown
} {
  const rawParameter = convertEditableParameterToRaw(editableParameter)
  delete rawParameter.file_path
  delete rawParameter.name
  return rawParameter
}

// See https://stackoverflow.com/questions/30106476/using-javascripts-atob-to-decode-base64-doesnt-properly-decode-utf-8-strings
function stringFromBase64(str: string): string {
  return browser
    ? // Going backwards: from bytestream, to percent-encoding, to original string.
      decodeURIComponent(
        window
          .atob(str)
          .split("")
          .map(function (c) {
            return "%" + ("00" + c.charCodeAt(0).toString(16)).slice(-2)
          })
          .join(""),
      )
    : Buffer.from(str, "base64").toString()
}

export async function getEmbeddedAndFileParameters(
  rootParameter: Parameter,
  name: string | undefined,
  parametersRepository: RepositoryConfig,
  units: Unit[],
): Promise<{
  embeddedParameter: Parameter
  fileParameter?: Parameter
  rawFileParameterYaml?: string
}> {
  const embeddedParameter = getEmbeddedParameter(rootParameter, name)
  if (embeddedParameter === undefined) {
    throw error(404, `Parameter "${name}" not found`)
  }

  if (embeddedParameter.file_path === undefined) {
    // Parameter is a Node, its index.yaml file doesn't exist and its content
    // has been automatically generated.
    return {
      embeddedParameter,
    }
  }

  const parameterUrl = `https://${
    parametersRepository.forge
  }/api/v4/projects/${encodeURIComponent(
    `${parametersRepository.group}/${parametersRepository.project}`,
  )}/repository/files/${encodeURIComponent(
    embeddedParameter.file_path,
  )}?ref=${encodeURIComponent(parametersRepository.branch)}`
  const parameterResponse = await fetch(parameterUrl, {
    headers: {
      Accept: "application/json",
    },
  })
  if (!parameterResponse.ok) {
    throw error(parameterResponse.status, await parameterResponse.text())
  }
  const fileParameterWrapper = await parameterResponse.json()
  // See https://developer.mozilla.org/en-US/docs/Glossary/Base64#the_unicode_problem
  const rawFileParameterYaml = stringFromBase64(fileParameterWrapper.content)
  const rawFileParameter = rawParameterFromYaml(rawFileParameterYaml) as {
    [key: string]: unknown
  }

  const [fileParameter, parameterError] = auditChain(
    auditRawParameterToEditable(
      units,
      // Since parameter YAML file (most of the times) doesn't contain the children,
      // inject the children IDs to be able to validate `order` metadata.
      embeddedParameter.class === ParameterClass.Node &&
        embeddedParameter.children !== undefined
        ? Object.keys(embeddedParameter.children)
        : undefined,
    ),
    auditRequire,
  )(strictAudit, rawFileParameter) as [Parameter, unknown]
  if (parameterError !== null) {
    console.error(
      `An error occurred when converting raw parameter at ${parameterUrl} to editable:`,
    )
    console.error(JSON.stringify(fileParameter, null, 2))
    console.error(JSON.stringify(parameterError, null, 2))
    throw error(400, `Invalid fileParameter "${name}"`)
  }
  fileParameter.file_path = embeddedParameter.file_path
  fileParameter.id = embeddedParameter.id
  fileParameter.name = embeddedParameter.name
  fileParameter.title = embeddedParameter.title
  fileParameter.titles = embeddedParameter.titles

  return {
    embeddedParameter,
    fileParameter,
    rawFileParameterYaml,
  }
}
