import type { Audit, Auditor } from "@auditors/core"

export function auditEditedAttribute<T>(
  audit: Audit,
  data: T,
  key: string,
  value: unknown,
  deleteNullish: boolean,
  errors: { [key: string]: unknown },
  ...auditors: Auditor[]
): [T, { [key: string]: unknown }] {
  let error = null
  for (const auditor of auditors) {
    ;[value, error] = auditor(audit, value)
    if (error !== null) {
      break
    }
  }
  if (error === null) {
    if (errors[key] !== undefined) {
      errors = { ...errors }
      delete errors[key]
    }
  } else if (error !== errors[key]) {
    errors = { ...errors, [key]: error }
  }
  if (error === null && deleteNullish && value == null) {
    if (data[key] !== undefined) {
      data = { ...data }
      delete data[key]
    }
  } else if (value !== data[key]) {
    data = { ...data, [key]: value }
  }
  return [data, errors]
}

export function errorAsKeyValueDictionary(error: unknown): {
  [key: string]: unknown
} {
  return error === undefined ||
    error === null ||
    typeof error === "string" ||
    Array.isArray(error) ||
    typeof error !== "object"
    ? {}
    : (error as {
        [key: string]: unknown
      })
}

export function* iterArrayWithErrors<T>(
  value: Iterable<T> | undefined | null,
  error: { [index: string]: unknown } | undefined | null,
): Generator<[T, unknown], void, unknown> {
  if (value != null) {
    if (
      error === undefined ||
      error === null ||
      typeof error === "string" ||
      Array.isArray(error) ||
      typeof error !== "object"
    ) {
      error = {}
    }
    for (const [index, item] of [...value].entries()) {
      yield [item, error[index]]
    }
  }
}
